﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz2Francisco.Models
{
    class comidas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int id { get; set; }

        [MaxLength(150)]
        public string nombre_comida { get; set; }

    }
}
