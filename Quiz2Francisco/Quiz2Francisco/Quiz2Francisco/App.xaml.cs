﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Quiz2Francisco
{
	public partial class App : Application
	{
        public static string urlBd;
        
		public App ()
		{
			InitializeComponent();

			MainPage = new Quiz2Francisco.MainPage();
		}

        //INICIO CONFIGURACION DE PAGINA 1
         
            public App(string url)
        {
            InitializeComponent();

            urlBd = url;

            MainPage = new Quiz2Francisco.MainPage();

        }

        //FIN CONFIGURACION DE PAGINA 1

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
