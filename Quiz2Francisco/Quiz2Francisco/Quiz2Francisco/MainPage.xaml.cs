﻿using Quiz2Francisco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Quiz2Francisco
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //INICIO DE BOTON PARA GUARDAR PERSONAS
        public void CrearComida(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryComida.Text))
            {
                DisplayAlert("Error", "Debe ingresar el campo de comida", "Ok");
            }
            else
            {
                comidas com = new comidas()
                {
                    nombre_comida = entryComida.Text
                };

                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
                {
                    connection.CreateTable<comidas>();

                    var result = connection.Insert(com);
                    if (result > 0)
                    {
                        DisplayAlert("Correcto", "sus datos han sido guardados", "Ok");
                    }
                    else
                    {
                        DisplayAlert("Error", "Sus datos no han sido guardados", "Ok");
                    }

                    List<comidas> listaComidas;
                    listaComidas = connection.Table<comidas>().ToList();

                    ListarComida.ItemsSource = listaComidas;
                }
                {

                }
            }
            entryComida.Text = "";
        }

        //FIN DE BOTON PARA GUARDAR PERSONAS

        //INICIO DE LISTAR PERSONAS

        /*protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<comidas> listaComidas;
                listaComidas = connection.Table<comidas>().ToList();

                ListarComida.ItemsSource = listaComidas;
            }
        }*/
        //FIN DE LISTAR PERSONAS
    }
}
